import unittest
from Interpreter_of_numbers import interpreter


class TestInterpreter(unittest.TestCase):
    def test_interpreter(self):
        self.assertEqual(interpreter(1), "I")


class MyError(Exception):
    def __init__(self, text, num):
        self.txt = text
        self.n = num


a = input("Input positive integer: ")

try:
    a = int(a)
    if a < 0:
        raise MyError("You give negative!", format(a))
    if a > 4000:
        raise MyError("Integer is out of range!", format(a))
except ValueError:
    print("Error type of value!")


if __name__ == '__main__':
    unittest.main()
